﻿using System;

namespace SampleSpace
{
    public class XXHashSequence
    {
        private object _seed;
        private ulong _index;
        private byte[] _hash_input;
        private int _cascading;

        public XXHashSequence()
        {

        }

        public XXHashSequence(object seed)
        {

        }

        [NoCascade]
        public void Seed()
        {
            ThrowIfCascading();
            throw new NotImplementedException();
        }

        public object GetSeed() {
            throw new NotImplementedException();
        }

        [NoCascade]
        public ulong Index
        {
            get { ThrowIfCascading();
                throw new NotImplementedException(); }
            set { ThrowIfCascading();
                throw new NotImplementedException(); }
        }

        [NoCascade]
        public void Reset()
        {
            _index = 0;
        }

        public ulong GetNextBlock()
        {
            throw new NotImplementedException();
        }

        public void Cascade(Action action)
        {
            ulong start_index = _index;
            _cascading++;
            try
            {
                action();
            }
            catch (Exception e)
            {
                _index = start_index;
                throw e;
            }
            finally
            {
                _cascading--;
            }

            if (_cascading <= 0)
            {
                _index = GetNextIndex(start_index);
            }
        }

        [NoCascade]
        public XXHashSequenceState State
        {
            get {
                ThrowIfCascading();
                return new XXHashSequenceState
                {
                    _seed = this._seed,
                    _index = this._index,
                    _hash_input = this._hash_input
                };
            }
            set
            {
                ThrowIfCascading();
                this._seed = value._seed;
                this._index = value._index;
                this._hash_input = value._hash_input;
            }
        }

        private void ThrowIfCascading()
        {
            if (_cascading > 0)
            {
                throw new InvalidOperationException("Operation forbidden while cascading.")''
            }
        }

        private static ulong GetNextIndex(ulong index)
        {
            return index + 1;
        }
    }
}
