﻿using System;

namespace SampleSpace
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
    public class NoCascadeAttribute : Attribute
    {

    }
}