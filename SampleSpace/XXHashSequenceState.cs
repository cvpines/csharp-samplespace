﻿namespace SampleSpace
{
    public struct XXHashSequenceState
    {
        internal object _seed;
        internal byte[] _hash_input;
        internal ulong _index;
    }
}
