﻿using System;

namespace SampleSpace
{
    public interface IRandomCompatibility
    {
        int Next();
        int Next(int max);
        int Next(int min, int max);

        void NextBytes(byte[] data);
        double NextDouble();
    }
}
